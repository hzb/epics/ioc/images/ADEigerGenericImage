FROM registry.hzdr.de/hzb/epics/ioc/images/adbaseimage

# Clone Detector Repository
RUN git clone -b main https://codebase.helmholtz.cloud/hzb/epics/support/ADEiger ${SUPPORT}/areaDetector/ADEiger
# Add the detector to RELEASE
RUN sed -i '/ADEIGER/s/^#//g' ${SUPPORT}/areaDetector/configure/RELEASE.local
# Install dependencies
RUN apt install libzmq3-dev -y
# Compile
RUN make -C ${SUPPORT}/areaDetector -j $(nproc)
# Change directory to where envPaths file is located
WORKDIR /opt/epics/support/areaDetector/ADEiger
